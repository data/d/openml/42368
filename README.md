# OpenML dataset: weather_ankara

https://www.openml.org/d/42368

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: KEEL - [original](https://sci2s.ugr.es/keel/dataset.php?cod=41) - Date unknown  
**Please cite**:   

**Weather Ankara dataset**

This file contains the weather information of Ankara from 01/01/1994 to 28/05/1998. From given features, the goal is to predict the mean
temperature.

**Attribute Information**

     1. Max_temperature real [23.0, 100.0]   
     2. Min_temperature real [-7.1, 65.5]   
     3. Dewpoint real [-3.1, 57.6]   
     4. Precipitation real [0.0, 4.0]   
     5. Sea_level_pressure real [29.46, 30.6]   
     6. Standard_pressure real [26.3, 27.18]   
     7. Visibility real [0.2, 11.5]   
     8. Wind_speed real [0.0, 18.0]   
     9. Max_wind_speed real [2.19, 57.4]   
     10. Mean_temperature real [7.9, 81.8]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42368) of an [OpenML dataset](https://www.openml.org/d/42368). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42368/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42368/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42368/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

